#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 25

int main()
{
    char str[N][75];
    char *strP[N];
    char *strIn;
    int i=0, j, ii;
    FILE *in, *out;
    if ((in = fopen("readingF.txt", "r")) == NULL)      // 䠩� ��� �⥭��
    {
        puts("error opening file\n");
        exit(0);
    }

    if ((out = fopen("writingF.txt", "w")) == NULL)  // 䠩� ��� �����
    {
        puts("error creating file\n");
        exit(0);
    }

    while (fgets(str[i], 75, in))      // �⥭�� ��ப
    {
        if (str[i][0] == '\n')        // �� ���⮩ ��ப�
            break;
        strP[i] = str[i++];  // ��।�� ����
    }

    for (ii = 0; ii < i - 1; ii++)    // ���஢�� 㪠��⥫��
    {
        for (j = 0; j < i - ii - 1; j++)
        {
            if (strlen(strP[j]) > strlen(strP[j + 1]))
            {
                strIn = strP[j];
                strP[j] = strP[j + 1];
                strP[j + 1] = strIn;
            }
        }
    }

    for (j = 0; j<i; j++)          // ������ ��ப �� ���ᠬ 㪠��⥫��
        fputs(strP[j], out);

    if (fclose(in) != 0 || fclose(out) != 0)
        puts("error closing files.\n");

    return 0;
}
